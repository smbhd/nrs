/**
 * Created by ChrisMatos on 5/23/2017.
 */

public class MaterialWrapper{


    public Boolean selected{get;set;}
    public Product2 material{get;set;}
    public Integer quantity{get;set;}
    public Double unitPrice{get;set;}
    public Boolean discountApplied{get;set;}
    public Double discountAmount{get;set;}

    public MaterialWrapper(Product2 mat){
        quantity = 0;
        selected = false;
        unitPrice = 0;
        material = mat;
        discountAmount = 0;
        discountApplied = false;
    }
    public Double total{get{
        return unitPrice * quantity;
    }}



}