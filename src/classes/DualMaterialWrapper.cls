/**
 * Created by ChrisMatos on 6/5/2017.
 */

public class DualMaterialWrapper{

   public MaterialWrapper materialWrapper{get;set;}
   public MaterialWrapper bulkMaterialWrapper{get;set;}

    public DualMaterialWrapper(MaterialWrapper matWrap, MaterialWrapper bulkWrap){
        materialWrapper = matWrap;
        bulkMaterialWrapper = bulkWrap;
    }
}