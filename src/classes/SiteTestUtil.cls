/**
 * Created by ChrisMatos on 5/25/2017.
 */
@IsTest
public with sharing class SiteTestUtil {

    public static void setupObjects(){
        Account acc = new Account();
        acc.Name = 'SMB Help Desk';
        insert acc;
        Contact con = new Contact();
        con.LastName = 'Doe';
        con.Email = 'doe@smbhd.com';
        con.AccountId = acc.id;
        insert con;
        Opportunity donation = new Opportunity();
        donation.AccountId = acc.id;
        donation.StageName = 'Pledged';
        donation.CloseDate = Date.today();
        donation.Payment_Method__c = 'Credit Card';
        donation.Name = 'Test Donation';
        insert donation;


    }
    public static User getGuestUser(){
        Id profileId = [SELECT id FROM Profile WHERE Name = 'Rosacea Profile'].id;
        return [SELECT id FROM User WHERE ProfileId = :profileId LIMIT 1];
    }
}