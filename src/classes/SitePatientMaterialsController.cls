/**
 * Created by ChrisMatos on 5/19/2017.
 */

public with sharing class SitePatientMaterialsController {
    public Contact aContact{get;set;}
    public Opportunity donation;
    public Map<String, List<Product2>> productsByFamily{get;set;}
    public MaterialWrapper[] patientMaterialWrappers{get;set;}
    public Boolean submitted{get;set;}
    public String street2{get;set;}

    public static final String NO_MATERIAL_ERROR = 'You must select at least one Material';
    public static final String NOT_A_MEMBER_INFO = 'We were unable to find you in our system. You must join as a member before requesting materials. Please contact us at (800)100-1000';
    public SitePatientMaterialsController() {
        aContact = new Contact();
        donation = new Opportunity();
        donation.Amount = 0;
        donation.Payment_Method__c = 'Credit Card';
        donation.StageName = 'Posted';
        donation.CloseDate = Date.today();
        submitted = false;

        populateMaterialWrappers();
    }
    public void populateMaterialWrappers(){
        patientMaterialWrappers = new List<MaterialWrapper>();

        for(Product2 prod : [SELECT id, Description, Name FROM Product2 WHERE Display_On_Patient_Form__c = TRUE]){
            MaterialWrapper wrapper = new MaterialWrapper(prod);
            patientMaterialWrappers.add(wrapper);
        }
    }
    public PageReference submit(){

        List<Id> selectedProductIds = new List<Id>();

        for(MaterialWrapper mw : patientMaterialWrappers){
            if(mw.selected && mw.material != null){
                selectedProductIds.add(mw.material.id);
            }
        }
        if(selectedProductIds.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, NO_MATERIAL_ERROR));
            return null;
        }
        if(String.isNotBlank(street2)){
            aContact.MailingStreet = aContact.MailingStreet + street2;
        }
        if(aContact.id == null){
            Id contactId = SiteUtil.findContact(aContact);
            if(contactId == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, NOT_A_MEMBER_INFO));
                return null;
            }
            aContact.id = contactId;
            try{
                update aContact;
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process your Contact record, talk to your administrator'));
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                return null;
            }

        }
        Contact queriedContact = [SELECT id, AccountId, Account.Name FROM Contact WHERE Id = :aContact.id];
        donation.AccountId = queriedContact.AccountId;
        donation.Name = queriedContact.Account.Name + ' - ' + Date.today().format();
        Savepoint sp = Database.setSavepoint();
        try{
            //probably send to some payment system here and wait for response
            insert donation;
            List<History_Item__c> items = new List<History_Item__c>();
            for(Id prodId : selectedProductIds){
                History_Item__c item = new History_Item__c();
                item.Quantity__c = 1;
                item.Material__c = prodId;
                items.add(item);
            }

            if(items.size() > 0){
                Mailing_List_History__c history = new Mailing_List_History__c();
                history.Mailing_List_Account__c = queriedContact.AccountId;
                history.Opportunity__c = donation.id;
                history.Date__c = Date.today();
                history.Contact__c = aContact.id;
                history.Method__c = 'Web';
                history.How_Requested__c = 'Webform';

                insert history;

                for(History_Item__c item : items){
                    item.Mailing_History__c = history.id;
                }
                insert items;
            }
            submitted = true;

        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process the Request, talk to your administrator'));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
        return null;
    }

}