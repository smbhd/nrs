/**
 * Created by ChrisMatos on 5/11/2017.
 */

public with sharing class SiteDonateFormController {

    public Contact theContact{get;set;}
    public Opportunity donation{get;set;}
    public String memberId{get;set;}

    public String street1{get;set;}
    public String street2{get;set;}
    public String city{get;set;}
    public String state{get;set;}
    public String zipCode{get;set;}

    public Boolean submitted{get;set;}

    public String expMonth{get;set;}
    public String expYear{get;set;}
    public String cardNum{get;set;}
    public String cardName{get;set;}


    public SiteDonateFormController(){
        donation = new Opportunity();
        theContact = new Contact();
        donation.Amount = 0;
        donation.Payment_Method__c = 'Credit Card';
        donation.StageName = 'Posted';
        donation.CloseDate = Date.today();
        donation.RecordTypeId = [SELECT id FROM RecordType WHERE Name = 'Donation' AND SobjectType = 'Opportunity'].id;

        submitted = false;
        if(ApexPages.currentPage().getParameters().get('join') == 'true'){
            donation.Amount = 25;
        }

    }
    public PageReference submit(){
        if(validateForm()){

            attachBillingInfoToContact();

            if(theContact.id == null){
                Id contactId = SiteUtil.findOrCreateContact(theContact);
                if(contactId == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to create Contact, talk to your administrator'));
                    System.debug('unable to find or create contact');
                    return null;
                }
                theContact.id = contactId;

            }
            Contact queriedContact = [SELECT id, AccountId, Account.Name FROM Contact WHERE Id = :theContact.id];


            try{
                //doing an upsert to prevent duplicate on failure...
                //maybe not the best way to accomplish this...
                if(donation.id == null){
                    donation.AccountId = queriedContact.AccountId;
                    donation.Name = queriedContact.Account.Name + ' - ' + Date.today().format();
                    insert donation;
                }

                doTransactionFuture(donation.AccountId, queriedContact.id, donation.id, cardName, cardNum, expMonth, expYear, donation.Amount);
//                if(t == null){
//                    //todo: indicate on donation payment didn't go through
//                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process transaction. Check payment method and try again'));
//                    return null;
//                }
                submitted = true;

            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process the Donation, talk to your administrator'));
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                return null;
            }
        }

        return null;

    }
    public Boolean validateForm(){
        List<String> errs = new List<String>();
        if(String.isBlank(theContact.Email)){
            errs.add('Email is required');
        }
        if(String.isBlank(theContact.LastName)){
            errs.add('Last Name is required');
        }
        if(String.isBlank(expMonth)){
            errs.add('Expiration Month is required');
        }
        if(String.isBlank(expYear)){
            errs.add('Expiration Year is required');
        }
        if(String.isBlank(cardNum)){
            errs.add('Card Number is required');
        }
        if(String.isBlank(cardName)){
            errs.add('Card Holder\'s Name is required');
        }
        if(donation.Amount <= 0){
            errs.add('Amount must be greater than 0');
        }
        if(String.isNotBlank(expMonth) && (Integer.valueOf(expMonth) > 12 || Integer.valueOf(expMonth) < 1)){
            errs.add('Please enter a valid Expiration Month');
        }
        if(errs.size() > 0){
            for(String err : errs){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, err));
            }
            return false;
        }
        return true;
    }

    @Future(callout=true)
    public static void doTransactionFuture(String accId, String conId, String donId, String name, String num, String expMonth, String expYear, Decimal amount){
        if(!Test.isRunningTest()){
            StripeConnector.makeTransaction(accId, conId, donId, name, num, expMonth, expYear, amount);
        }
    }


    public void attachBillingInfoToContact(){
        theContact.MailingStreet = street1 + (String.isBlank(street2) ? '' : '\r\n' + street2);
        theContact.MailingCity = city;
        theContact.MailingState = state;
        theContact.MailingPostalCode = zipCode;
    }

}