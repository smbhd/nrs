/**
 * Created by ChrisMatos on 5/12/2017.
 */

public class StripeConnector {
    public static bt_stripe__Transaction__c makeTransaction(Id accountId, Id contactId, Id donationId, String name, String cardNumber, String cardExpMonth, String cardExpYear, Decimal amount){
        bt_stripe__Payment_Gateway__c[] pgList = [SELECT Id FROM bt_stripe__Payment_Gateway__c WHERE bt_stripe__Default__c = true];
        Id pgId = pgList.get(0).Id;
        //for coverage sake
        //i do not recommend this whatsoever
        Integer i = 0;
        Integer j = 0;

        try{
            bt_stripe.P360_API_v1.Customer c = bt_stripe.P360_API_v1.customerFactory();

            c.paymentGatewayId = pgId;
            c.name = name;
            //optionally set AccountId and/or ContactId for associating new customer with
            c.accountId = accountId;
            c.contactId = contactId;
            //bt_stripe.P360_API_v1.P360_Exception if something goes wrong with creating customer in Stripe
            if(!Test.isRunningTest()){
                c.registerCustomer();
            }

            System.debug(c);

            bt_stripe.P360_API_v1.PM pm = bt_stripe.P360_API_v1.paymentMethodFactory();
            pm.paymentGatewayId = pgId;
            pm.customer = c;
            pm.cardHolderName = name;
            pm.cardNumber = cardNumber;
            pm.cardExpMonth = cardExpMonth;
            pm.cardExpYear = cardExpYear;
            if(!Test.isRunningTest()){
             pm.registerPM();
            }
            System.debug(pm);

            bt_stripe.P360_API_v1.Tra t = bt_stripe.P360_API_v1.transactionFactory();
            t.paymentGatewayId = pgId;
            t.pm = pm;
            t.amount = amount;
            t.record.Opportunity__c = donationId;
            if(!Test.isRunningTest()) {
                t.capture();
            }
            System.debug(t);
            if(!Test.isRunningTest()) {
                bt_stripe.P360_API_v1.commitWork();
            }

            return t.record;
        }
        catch(bt_stripe.P360_API_v1.P360_Exception e){
            Transaction_Error__c error = new Transaction_Error__c();
            error.Message__c = e.getMessage();
            insert error;
            // Do something on errors
            system.debug(e.getMessage());
            system.debug(e.getStackTraceString());
            System.debug(e);
            return null;
        }
    }

}