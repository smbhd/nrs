/**
 * Created by ChrisMatos on 5/12/2017.
 */

public with sharing class SiteUtil {
    public static Id findOrCreateContact(Contact customer){
        if(customer == null || customer.LastName == null){
            System.debug('No last name given or contact null');
            return null;
        }
        customer.id = findContact(customer);
        System.debug(customer.id);
        try{
            upsert customer;
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process the new Contact, talk to your administrator'));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
        return customer.Id;

    }
    public static Id findContact(Contact customer){
        List<Contact> contacts = [SELECT id, Email, Phone FROM Contact WHERE (LastName = :customer.lastName AND FirstName = :customer.firstName)];
        if(contacts.size() > 0){
            //found it
            if(contacts.size() == 1){
                return contacts[0].id;
            }
            for(Contact c : contacts){
                if(String.isNotBlank(customer.email) && c.Email == customer.Email){
                    return c.id;
                }
                if(String.isNotBlank(customer.phone) && c.Phone == customer.Phone){
                    return c.id;
                }
            }
            System.debug('Unable to match on email or phone, returning first one found');
            return contacts[0].id;
        }
        return null;
    }

    public Contact findContact(String memberId){
        return [SELECT id, Email, Phone, AccountId, Account.Name FROM Contact WHERE Name = :memberId LIMIT 1];
    }

}