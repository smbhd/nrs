/**
 * Created by ChrisMatos on 5/25/2017.
 */
@IsTest
public with sharing class SitePatientMaterialsControllerTest {
    @TestSetup
    public static void setup() {
        Product2 patientMat = new Product2();
        patientMat.Name = 'Patient Material';
        patientMat.IsActive = TRUE;
        patientMat.Display_On_Patient_Form__c = TRUE;
        patientMat.Display_on_Professional_Form__c = TRUE;
        patientMat.Physician_Request_Form_Section__c = 'Patient Education Materials';
        insert patientMat;
    }
    @IsTest
    public static void itShouldPopulateWithProducts(){
        SiteTestUtil.setupObjects();
        Integer productsOnPage = [SELECT COUNT() FROM Product2 WHERE Display_On_Patient_Form__c = TRUE];
        System.runAs(SiteTestUtil.getGuestUser()){
            SitePatientMaterialsController controller = new SitePatientMaterialsController();
            System.assertEquals(productsOnPage, controller.patientMaterialWrappers.size());

        }
    }
    @IsTest
    public static void itShouldCreateAccountIfContactDoesntExist(){}
    @IsTest
    public static void itShouldCreateMailingItems(){
        SiteTestUtil.setupObjects();
        System.runAs(SiteTestUtil.getGuestUser()) {
            SitePatientMaterialsController controller = new SitePatientMaterialsController();
            for(MaterialWrapper mw : controller.patientMaterialWrappers){
                mw.selected = true;
            }
            System.debug(controller.patientMaterialWrappers.size());
            controller.aContact.LastName = 'Test';
            controller.aContact.FirstName = 'SMB';
            controller.aContact.Email = 'testSMB@test.com';
            controller.submit();
            Opportunity donation = [SELECT id, (SELECT id FROM Mailing_History__r) FROM Opportunity WHERE AccountId IN (SELECT AccountId FROM Contact WHERE Id = :controller.aContact.Id)];
            System.assertEquals(1, donation.Mailing_History__r.size());
            //should have a line item for every patient material wrapper in list
            System.assertEquals(controller.patientMaterialWrappers.size(), [SELECT id FROM History_Item__c WHERE Mailing_History__c = :donation.Mailing_History__r.get(0).Id].size());
        }
    }
    @IsTest
    public static void itShouldPreventNoProductsBeingSelected(){
        SiteTestUtil.setupObjects();
        System.runAs(SiteTestUtil.getGuestUser()) {
            SitePatientMaterialsController controller = new SitePatientMaterialsController();
            controller.aContact.LastName = 'Test';
            controller.aContact.FirstName = 'SMB';
            controller.aContact.Email = 'testSMB@test.com';
            controller.submit();
            Boolean err = false;
            for(ApexPages.Message msg : ApexPages.getMessages()){
                if(msg.getDetail().contains(SitePatientMaterialsController.NO_MATERIAL_ERROR)){
                    err = true;
                }
            }
            System.assert(err);
        }
    }
}