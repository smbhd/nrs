/**
 * Created by ChrisMatos on 5/12/2017.
 */

public with sharing class SiteProfessionalMaterialsController {
    public Contact aContact{get;set;}
    public Opportunity donation;
    public Map<String, List<Product2>> productsByFamily{get;set;}
    public DualMaterialWrapper[] patientMaterialWrappers{get;set;}
    public MaterialWrapper[] professionalMaterialWrappers{get;set;}
    public MaterialWrapper[] articleReprintWrappers{get;set;}
    public MaterialWrapper[] selectedMaterials{get;set;}

    public final Integer NUM_FREE_BULK_PACKS{get;set;}

    public Integer freePacksUsed{get;set;}
    public Decimal discountAmount{get;set;}
    public String cardNumber{get;set;}
    public String cardName{get;set;}
    public String expirationMonth{get;set;}
    public String expirationYear{get;set;}

    private Boolean membershipSelected;
    public String companyName{get;set;}
    public String street2{get;set;}

    public MaterialWrapper professionalSubscription{get;set;}
    public final static String PROFESSIONAL_MEMBERSHIP = 'Professional Membership';
    public final String BULK_STRING = ' Bulk Pack';

    public Boolean submitted{get;set;}
    public String step{get;set;}
    public SiteProfessionalMaterialsController(){
        NUM_FREE_BULK_PACKS = (Integer)Professional_Subscription_Setting__c.getValues('Default').Free_Packs__c;
        if(NUM_FREE_BULK_PACKS == NULL || NUM_FREE_BULK_PACKS < 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Custom Setting on Professional_Subscription_Setting__c is invalid. If you are an administrator, Login to Salesforce and go to Build->Develop->Custom Settings to change'));
            return;
        }
        aContact = new Contact();
        donation = new Opportunity();
        donation.Amount = 0;
        donation.Payment_Method__c = 'Credit Card';
        donation.StageName = 'Posted';
        donation.CloseDate = Date.today();
        populatePatientMaterialWrappers();
        populateProfessionalMaterialWrappers();
        populateArticleReprints();
        submitted = false;
        step = 'step1';
        selectedMaterials = new List<MaterialWrapper>();
        membershipSelected = false;
        companyName = '';
        freePacksUsed = 0;

        Product2[] professionalProduct = [SELECT id, Name FROM Product2 WHERE Name = :PROFESSIONAL_MEMBERSHIP LIMIT 1];
        if(professionalProduct.size() > 0){
            PricebookEntry[] professionalPBE = [SELECT id, UnitPrice FROM PricebookEntry WHERE Product2Id = :professionalProduct[0].id AND Pricebook2.IsStandard = TRUE AND IsActive = TRUE LIMIT 1];
            if(professionalPBE.size() > 0){
                professionalSubscription = new MaterialWrapper(professionalProduct[0]);
                professionalSubscription.unitPrice = professionalPBE[0].UnitPrice;
                professionalSubscription.quantity = 1;

            }

        }
        System.debug(professionalSubscription);

    }
    public void populatePatientMaterialWrappers(){
        patientMaterialWrappers = new List<DualMaterialWrapper>();

        Product2[] patientMaterialProductsSorted = [SELECT id, Description, Name FROM Product2 WHERE Display_on_Professional_Form__c = TRUE AND Physician_Request_Form_Section__c = 'Patient Education Materials' AND IsActive = TRUE Order By Name Asc];
        Map<Id, PricebookEntry> entryByProductId = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe : [SELECT id, Product2Id, UnitPrice FROM PricebookEntry WHERE Product2Id IN :patientMaterialProductsSorted AND Pricebook2.IsStandard = TRUE]){
            entryByProductId.put(pbe.Product2Id, pbe);
        }
        //THE PRODUCTS MUST BE ORDERED BY NAME HERE
        //OR THIS WILL NOT WORK!
        for(Integer i = 0; i < patientMaterialProductsSorted.size(); i++){
            if(!patientMaterialProductsSorted.get(i).Name.endsWith(BULK_STRING)){
                MaterialWrapper matWrapper = new MaterialWrapper(patientMaterialProductsSorted.get(i));
                //wrapper.unitPrice = entryByProductId.get(wrapper.material.id).UnitPrice;
                DualMaterialWrapper dualWrap = new DualMaterialWrapper(matWrapper, null);
                if(i + 1 < patientMaterialProductsSorted.size() && patientMaterialProductsSorted.get(i+1).Name.endsWith(BULK_STRING)){
                    MaterialWrapper bulkWrapper = new MaterialWrapper(patientMaterialProductsSorted.get(i + 1));
                    if(entryByProductId.containsKey(bulkWrapper.material.id)) {
                        bulkWrapper.unitPrice = entryByProductId.get(bulkWrapper.material.id).UnitPrice;
                    }
                    dualWrap.bulkMaterialWrapper = bulkWrapper;
                    i++;
                }

                patientMaterialWrappers.add(dualWrap);
            }


        }

    }

    public void populateProfessionalMaterialWrappers(){
        professionalMaterialWrappers = new List<MaterialWrapper>();

        List<Product2> products = [SELECT id, Description, Name FROM Product2 WHERE Display_on_Professional_Form__c = TRUE AND Physician_Request_Form_Section__c = 'Professional Materials' AND IsActive = TRUE];

        Map<Id, PricebookEntry> entryByProductId = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe : [SELECT id, Product2Id, UnitPrice FROM PricebookEntry WHERE Product2Id IN :products AND Pricebook2.IsStandard = TRUE]){
            entryByProductId.put(pbe.Product2Id, pbe);
        }

        for(Product2 prod : products){
            MaterialWrapper wrapper = new MaterialWrapper(prod);
            if(entryByProductId.containsKey(prod.id)){
                wrapper.unitPrice = entryByProductId.get(prod.id).UnitPrice;
            }
            professionalMaterialWrappers.add(wrapper);
        }
    }
    public void populateArticleReprints(){
        articleReprintWrappers = new List<MaterialWrapper>();
        List<Product2> products = [SELECT id, Description, Name FROM Product2 WHERE Display_on_Professional_Form__c = TRUE AND Physician_Request_Form_Section__c = 'Article Reprints' AND IsActive = TRUE];

        Map<Id, PricebookEntry> entryByProductId = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe : [SELECT id, Product2Id, UnitPrice FROM PricebookEntry WHERE Product2Id IN :products AND Pricebook2.IsStandard = TRUE]){
            entryByProductId.put(pbe.Product2Id, pbe);
        }

        for(Product2 prod : products){
            MaterialWrapper wrapper = new MaterialWrapper(prod);
            if(entryByProductId.containsKey(prod.id)) {
                wrapper.unitPrice = entryByProductId.get(prod.id).UnitPrice;
            }
            articleReprintWrappers.add(wrapper);
        }

    }
    public void next(){
        if(step == 'step1'){
            membershipSelected = false;
            freePacksUsed = 0;
            discountAmount = 0;

            selectedMaterials = new List<MaterialWrapper>();
            if(professionalSubscription != null && professionalSubscription.selected == TRUE){
                selectedMaterials.add(professionalSubscription);
                membershipSelected = true;
            }
            for(DualMaterialWrapper wrapper : patientMaterialWrappers){
                if(wrapper.materialWrapper.selected){
                    wrapper.materialWrapper.quantity = 1;
                   selectedMaterials.add(wrapper.materialWrapper);
                }
                if(wrapper.bulkMaterialWrapper != null && wrapper.bulkMaterialWrapper.quantity > 0){
                    selectedMaterials.add(wrapper.bulkMaterialWrapper);
                    if(membershipSelected){
                        Double discountDouble = 0;
                        for(Integer i = 0; i < wrapper.bulkMaterialWrapper.quantity && freePacksUsed < NUM_FREE_BULK_PACKS; ++i){
                            freePacksUsed++;
                            discountAmount += wrapper.bulkMaterialWrapper.unitPrice;
                            wrapper.bulkMaterialWrapper.discountApplied = true;
                            discountDouble += wrapper.bulkMaterialWrapper.unitPrice;
                        }
                        wrapper.bulkMaterialWrapper.discountAmount = discountDouble;
                    }
                }
            }
            for(MaterialWrapper wrapper : professionalMaterialWrappers){

                if(wrapper.selected){
                    wrapper.quantity = 1;
                    selectedMaterials.add(wrapper);
                }
            }

            for(MaterialWrapper wrapper : articleReprintWrappers){
                if(wrapper.selected){
                    wrapper.quantity = 1;
                    selectedMaterials.add(wrapper);
                }
            }
            if(selectedMaterials.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'You must select at least one Material'));
                return;
            }
            step = 'step2';
            System.debug(selectedMaterials);
        }

    }
    public void back(){
        if(step == 'step2'){
            step = 'step1';
        }
    }
    public PageReference submit(){
        if(validForm()){
            if(membershipSelected == true){
                aContact.Category__c = 'Professional Membership';
            }
            Double totalPrice = getTotalPrice();
            if(String.isNotBlank(street2)){
                aContact.MailingStreet = aContact.MailingStreet + street2;
            }
            aContact.id = SiteUtil.findContact(aContact);
            try {
                if(aContact.id == null){

                    Account orgAccount = new Account();
                    orgAccount.Name = companyName;
                    insert orgAccount;
                    aContact.AccountId = orgAccount.id;
                    insert aContact;
                }
                else{
                    update aContact;
                }
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process your Contact record, talk to your administrator'));
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                return null;
            }
            Contact queriedContact = [SELECT id, AccountId, Account.Name FROM Contact WHERE Id = :aContact.id];
            String accId = queriedContact.AccountId;
            donation.AccountId = accId;
            donation.Name = queriedContact.Account.Name + ' - ' + Date.today().format();
            donation.Amount = totalPrice;
            Savepoint sp = Database.setSavepoint();
            try{
                //probably send to some payment system here and wait for response
                insert donation;

                if(totalPrice > 0){
                    SiteDonateFormController.doTransactionFuture(accId, aContact.id, donation.id, cardName, cardNumber, expirationMonth, expirationYear, totalPrice);
                }
                List<History_Item__c> items = new List<History_Item__c>();
                for(MaterialWrapper wrapper : selectedMaterials){

                    History_Item__c item = new History_Item__c();
                    item.Material__c = wrapper.material.id;
                    item.Quantity__c = wrapper.quantity;
                    items.add(item);

                }


                if(items.size() > 0){
                    Mailing_List_History__c history = new Mailing_List_History__c();
                    history.Mailing_List_Account__c = accId;
                    history.Opportunity__c = donation.id;
                    history.Date__c = Date.today();
                    history.Contact__c = aContact.id;
                    history.Method__c = 'Web';
                    history.How_Requested__c = 'Webform';


                    insert history;

                    for(History_Item__c item : items){
                        item.Mailing_History__c = history.id;
                    }
                    insert items;
                }
                submitted = true;

            }
            catch(Exception e){
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Failed to process the Donation, talk to your administrator'));
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                return null;
            }
        }

        return null;
    }
    public Boolean validForm(){
        List<String> errs = new List<String>();
        if(String.isBlank(aContact.Email)){
            errs.add('Email is required');
        }
        if(String.isBlank(aContact.LastName)){
            errs.add('Last Name is required');
        }
        if(String.isBlank(companyName)){
            errs.add('Company Name is required');
        }
        if(getTotalPrice() < 0){
            errs.add('Amount must not be less than 0');
        }
        if(errs.size() > 0){
            for(String err : errs){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, err));
            }
            return false;
        }
        return true;
    }
    public Double getSubtotal(){
        Double total = 0;
        for(MaterialWrapper wrapper : selectedMaterials){
            if(wrapper.selected || wrapper.quantity > 0){
                total += (wrapper.unitPrice * wrapper.quantity);
            }
        }
        return total;
    }
    public Double getTotalPrice(){
        Double total = 0;
        for(MaterialWrapper wrapper : selectedMaterials){
            if(wrapper.selected || wrapper.quantity > 0){
                total += (wrapper.unitPrice * wrapper.quantity);
            }
        }
        if(freePacksUsed > 0){
            total -= discountAmount;
        }
        return total;
    }



}