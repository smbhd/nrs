/**
 * Created by ChrisMatos on 5/25/2017.
 */
@IsTest
public with sharing class StripeConnectorTest {
    @IsTest(SeeAllData=true)
    public static void itShouldProcessADummyTransaction(){
        SiteTestUtil.setupObjects();
        bt_stripe__Stripe_Settings__c settings = bt_stripe__Stripe_Settings__c.getOrgDefaults();
        settings.bt_stripe__Disable_All_Triggers__c = true;
        upsert settings;
        String cardNumber = '4242424242424242';
        String cardName = 'Jane Doe';
        String expMonth = '12';
        String expYear = String.valueOf(Date.today().year() + 1);//some year in future
        Opportunity opp = [SELECT id FROM Opportunity LIMIT 1];
        Account acc = [SELECT id FROM Account LIMIT 1];
        Contact con = [SELECT id FROM Contact LIMIT 1];

        StripeConnector.makeTransaction(acc.id, con.id, opp.id, cardName, cardNumber, expMonth, expYear, 1);

    }
}