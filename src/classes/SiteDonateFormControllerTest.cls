/**
 * Created by ChrisMatos on 5/25/2017.
 */
@IsTest
public with sharing class SiteDonateFormControllerTest {
    @IsTest
    public static void itShouldCreateDonation(){
        SiteTestUtil.setupObjects();
        System.runAs(SiteTestUtil.getGuestUser()){
            SiteDonateFormController controller = new SiteDonateFormController();
            controller.theContact.LastName = 'Test New Person';
            controller.theContact.FirstName = 'First';
            controller.theContact.Email = 'test@test.com';
            controller.theContact.MailingStreet = '123 Test Street';
            controller.theContact.MailingCity = 'Chicago';
            controller.theContact.MailingState = 'IL';
            controller.theContact.MailingPostalCode = '60612';
            controller.cardName = 'First Test New Person';
            controller.cardNum = '1234566788';
            controller.expMonth = '1';
            controller.includeBilling = true;
            controller.expYear = String.valueOf(Date.today().year() + 1);
            controller.donation.Amount = 1;
            controller.submit();

            System.assertNotEquals(null, controller.theContact.id);
            Contact contactWithInfo = [SELECT id, AccountId FROM Contact WHERE Id = :controller.theContact.id];
            System.assertEquals(1, [SELECT id FROM Opportunity WHERE AccountId = :contactWithInfo.AccountId].size());
        }
    }
    @IsTest
    public static void itShouldFindExistingContact(){
        SiteTestUtil.setupObjects();
        Contact con = [SELECT id, FirstName, LastName, Email FROM Contact LIMIT 1];
        System.runAs(SiteTestUtil.getGuestUser()) {
            SiteDonateFormController controller = new SiteDonateFormController();
            controller.theContact.LastName = con.LastName;
            controller.theContact.FirstName = con.FirstName;
            controller.theContact.Email = con.Email;
            controller.cardName = 'First Test New Person';
            controller.cardNum = '1234566788';
            controller.expMonth = '1';
            controller.expYear = String.valueOf(Date.today().year() + 1);
            controller.donation.Amount = 1;
            controller.submit();
            System.assertEquals(con.id, controller.theContact.id);
        }

    }
    @IsTest
    public static void itShouldNotAllowInvalidEntries(){
        System.runAs(SiteTestUtil.getGuestUser()) {
            SiteDonateFormController controller = new SiteDonateFormController();
            controller.theContact.FirstName = 'Test Name';
            controller.submit();
        }
    }

}