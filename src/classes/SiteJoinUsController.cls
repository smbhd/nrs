/**
 * Created by ChrisMatos on 5/19/2017.
 */

public with sharing class SiteJoinUsController {
    public PageReference goToDonatePage(){

        PageReference ref = Page.SiteDonateForm;
        ref.getParameters().put('join', 'true');

        return ref;
    }
}