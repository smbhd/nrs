/**
 * Created by ChrisMatos on 5/25/2017.
 */
@IsTest
public with sharing class SiteProfessionalMaterialsControllerTest {
    @TestSetup
    public static void setup(){
        Product2 profMat = new Product2();
        profMat.IsActive = TRUE;
        profMat.Name = SiteProfessionalMaterialsController.PROFESSIONAL_MEMBERSHIP;
        profMat.Display_on_Professional_Form__c = TRUE;
        profMat.Physician_Request_Form_Section__c = 'Professional Materials';
        insert profMat;
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id = profMat.id;
        pbe.UnitPrice = 125;
        insert pbe;
        Product2 article = new Product2();
        article.IsActive = TRUE;
        article.Name = 'Article';
        article.Display_on_Professional_Form__c = TRUE;
        article.Physician_Request_Form_Section__c = 'Article Reprints';
        insert article;
        Product2 patientMat = new Product2();
        patientMat.Name = 'Patient Material';
        patientMat.IsActive = TRUE;
        patientMat.Display_On_Patient_Form__c = TRUE;
        patientMat.Display_on_Professional_Form__c = TRUE;
        patientMat.Physician_Request_Form_Section__c = 'Patient Education Materials';
        insert patientMat;


        Professional_Subscription_Setting__c setting = new Professional_Subscription_Setting__c();
        setting.Free_Packs__c = 10;
        setting.Name = 'Default';
        insert setting;
    }
    @IsTest
    public static void itShouldPopulateWithProducts(){
        SiteTestUtil.setupObjects();
        System.runAs(SiteTestUtil.getGuestUser()){
            SiteProfessionalMaterialsController controller = new SiteProfessionalMaterialsController();
            System.assert(controller.patientMaterialWrappers.size() > 0);
            System.assert(controller.professionalMaterialWrappers.size() > 0);
            System.assert(controller.articleReprintWrappers.size() > 0);

        }
    }
    @IsTest
    public static void itShouldSubmitOppAndLineItems(){

    }
    @IsTest
    public static void itShouldAllowFreeBulkPacksWithProfessionalMemebership(){
        SiteTestUtil.setupObjects();
        System.runAs(SiteTestUtil.getGuestUser()){
            SiteProfessionalMaterialsController controller = new SiteProfessionalMaterialsController();
            System.assertEquals(10,controller.NUM_FREE_BULK_PACKS);

            controller.professionalSubscription.selected = true;

            for(MaterialWrapper mw : controller.professionalMaterialWrappers){
                mw.selected = true;
                mw.quantity = 1;
            }
            controller.next();
            System.assertNotEquals(0,controller.discountAmount);

        }
    }
}